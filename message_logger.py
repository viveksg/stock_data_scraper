from datetime import datetime
import logging
class Message_logger:

    @staticmethod
    def log_error(msg):
        msg = Message_logger.get_current_time_str()+"Info: "+msg
        logging.error(msg)

    @staticmethod
    def log_warning(msg):
        msg = Message_logger.get_current_time_str()+"Info: "+msg
        logging.warning(msg)

    @staticmethod
    def log_info(msg):
        msg = Message_logger.get_current_time_str()+"Info: "+msg
        print(msg)
        logging.info(msg)

    @staticmethod
    def get_current_time_str():
        curr_time_str = "[" + str(datetime.now()) +"] "
        return curr_time_str