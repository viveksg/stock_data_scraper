from constants import Constants
from os import path
from message_logger import Message_logger
from utils import Utils
import json


class ConfigurationManager:
    base_config_file = None
    stock_config_file = None
    base_config = None
    stock_config = []
    default_base_config = {
        "stock_list_file_location": "stock_list/stock_list.txt",
        "result_file_location": "results",
        "api_key": "",
        "write_daily_data_to_seperate_files": 0,
        "request_delay": 15,
        "batch_delay": 30,
        "exception_delay": 60,
        "api_interval": 1,
        "print_values": 1,
    }
    stock_list = []
    api_key = None
    create_separate_file = False
    result_dir = ""

    def __init__(self):
        self.base_config_file = Constants.BASE_CONFIG_FILE_LOCATION
        self.stock_config_file = Constants.STOCK_CONFIG_FILE_LOCATION

    def prepare_configs(self):
        self.prepare_base_config()
        self.result_dir = self.base_config[Constants.STOCK_CONFIG_RESULT_FILE]
        self.create_separate_file = False if self.base_config[
                                                 Constants.MAKE_SEPARATE_FILE] == 0 else True
        self.api_key = self.base_config[Constants.CONFIG_API_KEY]
        self.prepare_stock_config()

    def prepare_base_config(self):
        try:
            with open(self.base_config_file, "r") as b_config:
                self.base_config = json.load(b_config)
        except Exception:
            Message_logger.log_error("no file found in configurations directory, default configs will be loaded")
            self.default_base_config[Constants.CONFIG_API_KEY] = input("Enter alphavantage API key: ")
            self.default_base_config[Constants.STR_API_INTERVAL] = input("Enter api_interval (1,5,15,30,60): ")
            self.default_base_config[Constants.STR_PRINT_VALUES] = int(
                input("Do you want to print results, press 1 for yes, 0 for no: "))
            self.base_config = self.default_base_config

        self.prepare_stock_list(self.base_config[Constants.CONFIG_STOCK_LIST_FILE_LOC])

    def prepare_stock_list(self, loc):
        with open(loc, "r") as stock_list_file:
            for line in stock_list_file:
                self.stock_list.append(line.strip())

    def prepare_stock_config(self):
        try:
            with open(self.stock_config_file, "r") as sconfig_file:
                found_config = json.load(sconfig_file)
        except Exception as exc:
            found_config = json.loads("{}")
            Message_logger.log_warning("stock_config not set, will be set to defaults")
        for stock in self.stock_list:
            if stock in found_config:
                current_config = found_config[stock]
            else:
                current_config = json.loads("{}")
            temp = dict()
            temp[Constants.STOCK_CONFIG_NAME] = stock
            temp[Constants.STOCK_CONFIG_MARKET_TZ] = Constants.DEFAULT_MARKET_TZ
            temp[Constants.STOCK_CONFIG_LAST_UPDATED] = current_config[
                Constants.STOCK_CONFIG_LAST_UPDATED] if Constants.STOCK_CONFIG_LAST_UPDATED in current_config else 0
            temp[Constants.STOCK_CONFIG_RESULT_DIR] = Utils.get_result_file_directory_path(self.result_dir, stock,
                                                                                           self.create_separate_file)
            temp[Constants.STOCK_CONFIG_RESULT_FILE] = Utils.get_expected_date_based_file_name(Constants.DEFAULT_MARKET_TZ)
            temp[Constants.MODE] = Constants.STOCK_CONFIG_FILE_MODE
            temp[Constants.STOCK_MAKE_FULL_REQUEST] = Utils.requires_full_request(
                temp[Constants.STOCK_CONFIG_LAST_UPDATED], Constants.DEFAULT_MARKET_TZ)
            self.stock_config.append(temp)

    def save_stock_config(self, config):
        with open(self.stock_config_file, 'w') as stock_config:
            temp_data = dict()
            for stock in config:
                temp_data[stock[Constants.STOCK_CONFIG_NAME]] = stock
            json.dump(temp_data, stock_config, sort_keys=True, indent=4)
