import calendar
import time
import json
import urllib.request
from threading import Thread
from constants import Constants
from queue import Queue
from csv_file_handler import CSVFileHandler
from message_logger import Message_logger
from configuration_manager import ConfigurationManager
from utils import Utils
from os import path


class Schedular(Thread):
    stock_index_data = None
    file_handler = None
    keep_running = None
    successfully_fetched_stocks = []
    unsuccessfully_fetched_stocks = []
    process_queue = None
    delay = 0
    api_key = None
    config_manager = None
    request_delay = 0
    batch_delay = 0
    exception_delay = 0
    api_interval = 0
    data_array_parameter = 0
    printing_enabled = 0

    def __init__(self):
        Thread.__init__(self)
        self.config_manager = ConfigurationManager()
        self.config_manager.prepare_configs()
        self.stock_index_data = self.config_manager.stock_config
        self.file_handler = CSVFileHandler()
        self.keep_running = True
        self.job_queue = Queue()
        if self.config_manager.api_key is None:
            raise ValueError("API KEY is not provided")
        self.api_key = self.config_manager.api_key
        self.set_delays()
        self.prepare_api_interval_data()
        self.printing_enabled = True if self.config_manager.base_config[Constants.STR_PRINT_VALUES] == 1 else False

    def run(self):
        while self.keep_running:
            self.handle_requests()
            self.delay = self.get_any_required_delay()
            if self.delay > 0:
                time.sleep(self.delay)
                self.delay = 0

    def handle_requests(self):
        for stock in self.stock_index_data:
            process_successful = True
            stock[Constants.STOCK_MAKE_FULL_REQUEST] = Utils.requires_full_request(
                stock[Constants.STOCK_CONFIG_LAST_UPDATED], Constants.DEFAULT_MARKET_TZ)
            stock_name = stock[Constants.STOCK_CONFIG_NAME]
            Message_logger.log_info("Fetching data for " + stock_name + ", from alphavantage")
            data = self.get_data_from_alphavantage(stock_name, stock[Constants.STOCK_MAKE_FULL_REQUEST])
            if data is None:
                self.unsuccessfully_fetched_stocks.append(stock)
                time.sleep(Constants.WAIT_TIME)
                continue
            result_file_loc = path.join(stock[Constants.STOCK_CONFIG_RESULT_DIR],
                                        stock[Constants.STOCK_CONFIG_RESULT_FILE])
            result_data = self.process_data(data, stock_name, stock[Constants.STOCK_CONFIG_LAST_UPDATED],
                                            result_file_loc)
            if self.file_handler.write(stock[Constants.STOCK_CONFIG_RESULT_DIR],
                                       stock[Constants.STOCK_CONFIG_RESULT_FILE], stock[Constants.MODE], result_data,
                                       Constants.CSV_FILE_HEADERS):
                stock[Constants.STOCK_CONFIG_LAST_UPDATED] = result_data[Constants.MAX_TIMESTAMP]
                Message_logger.log_info(
                    result_data[Constants.STR_SELECTED_TUPLES] + "/" + result_data[
                        Constants.STR_TOTAL_TUPLES] + " tuples for " + stock_name + " written to " + path.join(
                        stock[Constants.STOCK_CONFIG_RESULT_DIR], stock[
                            Constants.STOCK_CONFIG_RESULT_FILE]))
            else:
                process_successful = False
                self.unsuccessfully_fetched_stocks.append(stock)
                Message_logger.log_error(
                    "Unable to write data for " + stock_name + ", at" + stock[Constants.STOCK_CONFIG_RESULT_FILE])
            if process_successful:
                self.successfully_fetched_stocks.append(stock)
            time.sleep(Constants.WAIT_TIME)

        self.prepare_next_batch_requests()

    def get_data_from_alphavantage(self, stock_name, get_full_data):
        url = Constants.BASE_URL + "&" + Constants.PARAM_SYMBOL + "=" + stock_name \
              + "&" + Constants.API_KEY_PARAM + "=" + self.api_key \
              + "&" + Constants.PARAM_INTERVAL + "=" + str(self.api_interval) + Constants.STR_MIN
        url += "&" + Constants.PARAM_OUTPUT_SIZE + "=" + Constants.VAL_OUTPUT_SIZE if get_full_data else ""
        try:
            with urllib.request.urlopen(url) as response:
                return response.read()
        except Exception as exc:
            Message_logger.log_error("Request Rejected from server, will reattempt shortly")
            self.impose_exception_wait()
            return None

    def process_data(self, stock_data, stock_name, last_update_timestamp, file_loc):
        json_data = json.loads(stock_data)
        if not Constants.META_DATA in json_data:
            Message_logger.log_error(
                "Data for " + stock_name + " not fetched successfully from server will reattempt soon")
            return None
        time_series_data_present = True
        data_array_parameter = self.data_array_parameter
        if self.data_array_parameter not in json_data:
            time_series_data_present = False
            for array_param in Constants.ARRAY_DATA_NAME_LIST:
                if array_param in json_data:
                    data_array_parameter = array_param
                    Message_logger.log_info(
                        self.data_array_parameter + " not present, but found: " + data_array_parameter)
                    time_series_data_present = True
                    break
        if not time_series_data_present:
            Message_logger.log_error(
                "Data for " + stock_name + " not fetched successfully from server will reattempt soon")
            return None

        time_series = json_data[data_array_parameter]
        results = dict()
        max_time_stamp = last_update_timestamp
        csv_map = []
        if self.printing_enabled:
            self.print_values(time_series)
        added_records = 0
        for item in time_series:
            json_value = time_series[item]
            item_timestamp = Utils.get_unix_epoch_from_str(Constants.DATETIME_FORMAT, item, Constants.DEFAULT_MARKET_TZ)
            temp_data = dict()
            temp_data[Constants.CSV_FIELD_MARKET_TIMESTAMP] = item
            temp_data[Constants.CSV_FIELD_LOCAL_TIMESTAMP] = Utils.get_date_time_from_timestamp(item_timestamp)
            temp_data[Constants.CSV_FIELD_STOCK_SYMBOL] = stock_name
            for data_value in Constants.JSON_CSV_FIELDS_MAP:
                if Constants.JSON_CSV_FIELDS_MAP[data_value] in json_value:
                    temp_data[data_value] = json_value[Constants.JSON_CSV_FIELDS_MAP[data_value]]
                else:
                    temp_data[data_value] = "NaN"
            if item_timestamp <= last_update_timestamp:
                continue
            added_records += 1
            max_time_stamp = item_timestamp if item_timestamp > max_time_stamp else max_time_stamp
            csv_map.append(temp_data)
        current_csv_data = self.file_handler.get_file_data(file_loc,Constants.CSV_FILE_HEADERS)
        for val in current_csv_data:
            csv_map.append(val)
        results[Constants.MAX_TIMESTAMP] = max_time_stamp
        results[Constants.CSV_DATA] = csv_map
        results[Constants.STR_TOTAL_TUPLES] = str(len(time_series))
        results[Constants.STR_SELECTED_TUPLES] = str(added_records)
        return results

    def prepare_next_batch_requests(self):
        self.stock_index_data.clear()
        for stock in self.unsuccessfully_fetched_stocks:
            self.stock_index_data.append(stock)
        for stock in self.successfully_fetched_stocks:
            self.stock_index_data.append(stock)
        self.unsuccessfully_fetched_stocks.clear()
        self.successfully_fetched_stocks.clear()
        self.config_manager.save_stock_config(self.stock_index_data)

    def get_any_required_delay(self):
        return 0 if len(self.unsuccessfully_fetched_stocks) == 0 else self.batch_delay

    def set_delays(self):
        self.exception_delay = self.config_manager.base_config[Constants.STR_EXCEPTION_DELAY]
        self.request_delay = self.config_manager.base_config[Constants.STR_REQUEST_DELAY]
        self.batch_delay = self.config_manager.base_config[Constants.STR_BATCH_DELAY]

    def impose_exception_wait(self):
        Message_logger.log_info("Pausing request process for " + str(self.exception_delay) + " seconds")
        time.sleep(self.exception_delay)
        Message_logger.log_info("Resumed request process...")

    def prepare_api_interval_data(self):
        self.api_interval = self.config_manager.base_config[Constants.STR_API_INTERVAL]
        self.data_array_parameter = Constants.TIME_SERIES + "(" + str(self.api_interval) + Constants.STR_MIN + ")"

    def stop_schedular(self):
        self.keep_running = False

    def print_values(self, values):
        if values is None or len(values) == 0:
            print("Status: No Data Received from server")
            return
        print("Status: Successful, Data Received from server")
        iterator = iter(values)
        key = next(iterator)
        print("Latest Record: " + str(values[key]))
