import csv
from os import makedirs
from os import path
from constants import Constants


class CSVFileHandler:

    def write(self, file_dir, file_loc, mode, result_data, headers):
        full_file_path = path.join(file_dir, file_loc)
        # write_headers = True if not path.exists(full_file_path) else False
        try:
            if not path.exists(file_dir):
                makedirs(file_dir, exist_ok=True)
            data_array = result_data[Constants.CSV_DATA]
            with open(full_file_path, mode, newline='') as output_csv:
                csv_writer = csv.DictWriter(output_csv, fieldnames=headers, dialect=csv.excel)
                csv_writer.writeheader()
                for data in data_array:
                    csv_writer.writerow(data)
        except Exception as exception:
            print(exception)
            return False
        return True

    def get_file_data(self, file_loc, headers):
        if not path.exists(file_loc):
            return []
        try:
            result = []
            with open(file_loc, "r", newline='') as csv_file:
                csv_reader = csv.DictReader(csv_file, fieldnames=headers, dialect=csv.excel)
                for row in csv_reader:
                    result.append(row)
            result.pop(0)
            return result
        except Exception as exc:
            return []
