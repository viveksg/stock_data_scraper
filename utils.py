from constants import Constants
from datetime import datetime, timedelta
from pytz import timezone
import pytz
import calendar
import time
import os
class Utils:

    @staticmethod
    def get_expected_file_name(stock_name, create_separate_file, market_timezone):
        prefix = stock_name if create_separate_file else Constants.DEFAULT_SINGLE_FILE_PREFIX
        suffix = Utils.get_current_date(market_timezone, Constants.SUFFIX_FORMAT)
        return os.path.join(prefix, (suffix + Constants.RESULT_FILE_EXTENSION))

    @staticmethod
    def get_expected_date_based_file_name(market_timezone):
        return Utils.get_current_date(market_timezone,Constants.SUFFIX_FORMAT)+Constants.RESULT_FILE_EXTENSION

    @staticmethod
    def get_result_file_directory_path(base_result_dir, stock_name, create_separate_file):
        result_dir = stock_name if create_separate_file else Constants.DEFAULT_SINGLE_FILE_PREFIX
        return os.path.join(base_result_dir,result_dir)

    @staticmethod
    def requires_full_request(last_update, tzone):
        current_time = calendar.timegm(time.gmtime())
        return current_time - last_update > Constants.SECONDS_DIFFERENCE_FOR_FULL_REFRESH

    @staticmethod
    def get_unix_epoch_from_str(format, date_time, time_zone):
        market_time_zone = timezone(time_zone)
        dtime = datetime.strptime(date_time, format)
        target_tz_time = market_time_zone.localize(dtime, is_dst=None)
        utc_start = datetime(1970, 1, 1, tzinfo=pytz.utc)
        unix_timestamp = (target_tz_time - utc_start).total_seconds()
        return int(unix_timestamp)

    @staticmethod
    def get_date_time_from_timestamp(time_stamp):
        return datetime.fromtimestamp(time_stamp)

    @staticmethod
    def get_current_date(time_zone, dt_format):
        curr_time = calendar.timegm(time.gmtime())
        utc_dtime = pytz.utc.localize(datetime.utcfromtimestamp(curr_time))
        target_time_zone = timezone(time_zone)
        target_tz_dtime = utc_dtime.astimezone(target_time_zone)
        return target_tz_dtime.strftime(dt_format)
